import dishes from './menu.js';

class Order {
  constructor (itemsMap) {
    this.myMap = itemsMap;

    const list = [];
    for (const key of this.myMap) {
      list.push([key, this.myMap[key]]);
    }

    this.itemDetails = list.map((item) => {
      let aName = '';
      let aPrice = 0;
      for (let i = 0; i < 4; i++) {
        if (dishes[i].id === item[0][0]) {
          aName = dishes[i].name;
          aPrice = dishes[i].price;
          break;
        }
      }
      return {
        id: item[0][0],
        name: aName,
        price: aPrice,
        count: item[0][1]
      };
    });
  }

  get itemsDetails () {
    return this.itemDetails;
  }

  get totalPrice () {
    return this.itemDetails.reduce((total, item) => total + item.price * item.count, 0);
  }
}

export default Order;
