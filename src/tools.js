function parseInputToMap (selectedItems) {
  const input = selectedItems.split(',');
  const myMap = new Map();
  for (const s of input) {
    const toMap = s.split(' x ');
    myMap.set(toMap[0], parseInt(toMap[1]));
  }
  return myMap;
}

export { parseInputToMap };
