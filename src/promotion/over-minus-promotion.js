import Promotion from './promotion.js';

class OverMinusPromotion extends Promotion {
  constructor (order) {
    super(order);
    this._type = '满30减6元';
  }

  get type () {
    return this._type;
  }

  discount () {
    if (this.order.totalPrice <= 30) return 0;
    else return 6;
  }
}

export default OverMinusPromotion;
